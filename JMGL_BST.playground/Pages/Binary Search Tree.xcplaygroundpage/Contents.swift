/*:

# Binary Search Tree

**Moi Gutierrez**
Apr 2016

swift 2.2

//: ![The real head of the household?](tree.png)

*/

/*:
---
*/

class BinarySearchTreeNode {

	// Basic properties
	var data:Int? = nil
	var leftNode:BinarySearchTreeNode?
	var rightNode:BinarySearchTreeNode?

	// More advanced properties
	var parentNode:BinarySearchTreeNode?

	private var _isLeftChild:Bool = true
	var isLeftChild:Bool {

		get{

			print("node is a \(_isLeftChild ? "left" : "right") child.")
			return _isLeftChild
		}

		set (newValue) {

			_isLeftChild = newValue
		}
	}

	private var _isLeafNode:Bool = true
	var isLeafNode:Bool {
		get{

			print("node is \(_isLeafNode ? "a" : "not a") leaf node.")
			return _isLeafNode

		}
		set (newValue) {

			_isLeafNode = newValue
		}

	}

	private var _depth:Int = 0
	var depth:Int {
		get{

//			print("node depth is \(_depth) leaf node")
			return _depth

		}
		set (newValue){

			_depth = newValue

		}
	}

	init() {

		//        print("node has been created")

	}

	deinit {

		//        print("node has been destroyed")

	}

	convenience init(data: Int){

		self.init()
		self.data = data

		//        print("node has been conveniently created")


	}

	convenience init(data: Int, level: Int, isLeafNode: Bool, isLeftChild: Bool){

		self.init()
		self.data = data
		self.depth = level
		self.isLeafNode = isLeafNode
		self.isLeftChild = isLeftChild

		//        print("node has been conveniently created")


	}
}

/*:
---
*/

private protocol BinarySearchTreeProtocol {

	func insert(data: Int) -> BinarySearchTreeNode?
	func search(data: Int) -> BinarySearchTreeNode?
	func remove(data: Int)
	func isContained(data: Int)  -> Bool

	func findMin() -> Int?
	func findMax() -> Int?

}

/*:
---
*/

/**
This is my first library on BST.

By definition a BST is an ordered set so NO DUPLICATES by default.

I commented out print() statements to improve performance.

*/

public class BinarySearchTree: BinarySearchTreeProtocol {

	var root:BinarySearchTreeNode?

	private var _maxDepth:Int = -1
	var maxDepth:Int {
		get{

			print("tree max depth is \(_maxDepth).")
			return _maxDepth

		}
		set (newValue){

			_maxDepth = newValue
			
		}
	}

	var leafNodes:[BinarySearchTreeNode] = [BinarySearchTreeNode]()

	// This property is not the default behaviour of a BST, look at NOTES for explanation.
	var allowDuplicates:Bool = false

	// These functions abstract interaction with BinarySearchTree

	/**
	This function .

	@param *data* must be of type **Int**.

	@return an **Int**.
	*/

	internal func insert(data: Int) -> BinarySearchTreeNode? { return self.insert(&root, data: data) }

	internal func search(data: Int) -> BinarySearchTreeNode? { return self.search(root, data: data) }

	internal func remove(data: Int) { self.remove(&root, data: data) }

	internal func isContained(data: Int)  -> Bool { return self.isContained(root, data: data) }

	// Min & Max
	internal func findMin() -> Int? { return self.findMin(root)?.data }

	internal func findMax() -> Int? { return self.findMax(root)?.data }

	// To insert data in BinarySearchTree, returns root node.
	private func insert(inout node: BinarySearchTreeNode?, data: Int) -> BinarySearchTreeNode? {

		var returnedNode:BinarySearchTreeNode?

		if node == nil {

			node = BinarySearchTreeNode(data: data)

			if self.root == nil { // initialize empty tree

				self.root = node

			}

		}

		else if data == node!.data  && !allowDuplicates {

			return node

		}
			// if data to be inserted is less, insert in leftNode subtree.
		else if data <= node!.data {

			returnedNode = insert(&node!.leftNode , data: data)
			// set child side
			returnedNode?.isLeftChild = true
			// set left node
			node!.leftNode = returnedNode

		}

			// else, insert in rightNode subtree.
		else if data >= node!.data {

			returnedNode = insert(&node!.rightNode,data: data)
			// set child side
			returnedNode?.isLeftChild = false
			// set right node
			node!.rightNode = returnedNode

		}

		// check if this node is a leaf Node
		if  node?.leftNode == nil && node?.rightNode == nil {

			returnedNode?.isLeafNode = true

		}

		if let returnedNode = returnedNode {

			// set node as not a leaf
			node?.isLeafNode = false
			// set level for node
			returnedNode.depth = node!.depth + 1
			// set parent node
			returnedNode.parentNode = node

			// update max tree depth
			if returnedNode.depth > maxDepth {

				maxDepth = returnedNode.depth

			}

		}

		return node

	}

	// To remove an element in tree
	private func remove(inout node: BinarySearchTreeNode?, data: Int) -> Bool {

		if node == nil {

			return false

		} else {

			return true

		}


	}

	// To search an element in BinarySearchTree, returns true if element is found
	private func isContained(node: BinarySearchTreeNode?,data: Int) -> Bool {

		if node == nil {

			return false

		}

		else if node!.data == data {

			return true

		}

		else if data <= node!.data {

			return isContained(node!.leftNode,data: data)

		}

		else {

			return isContained(node!.rightNode,data: data)

		}

	}

	// To search an element in BinarySearchTree, returns element if found
	private func search(node: BinarySearchTreeNode?,data: Int) -> BinarySearchTreeNode? {

		if node == nil{

			return nil

		}

		else if node!.data == data {

			return node!

		}

		else if data <= node!.data {

			return search(node!.leftNode,data: data)

		}

		else {

			return search(node!.rightNode,data: data)

		}

	}

	private func findMin(node: BinarySearchTreeNode?) -> BinarySearchTreeNode? {

		// if there are no nodes then there is no Min
		if node == nil {

			return nil

		}

		if  let node = node!.leftNode {

			return findMin(node)

		} else {

			return node

		}

	}

	private func findMax(node: BinarySearchTreeNode?) -> BinarySearchTreeNode? {

		// if there are no nodes then there is no Min
		if node == nil {

			return nil

		}

		if  let node = node!.rightNode {

			return findMax(node)

		} else {

			return node

		}


	}

}

/*================================================================
								Testing
*///==============================================================

import Foundation

extension Int {

	// Generates a random 'Int' inside of the closed interval.
	public static func random(interval: ClosedInterval<Int>) -> Int {

		return interval.start + Int(arc4random_uniform(UInt32(interval.end - interval.start + 1)))

	}

}

var tree = BinarySearchTree()

for i in 1...100 {

    tree.insert(Int.random(-100...100))

}

if false {
	let root = tree.insert(1)
	root?.parentNode?.data

	let node = tree.insert(17)
	node?.parentNode?.data

	tree.insert(22)
	tree.insert(33)
	tree.insert(8)
	tree.insert(9)
}

var c:Bool = tree.isContained(17)
var k = tree.search(1)

tree.maxDepth
let min = tree.findMin()
let max = tree.findMax()

k?.parentNode?.data
k?.data
k?.depth
k?.isLeftChild
k?.isLeafNode
//: [Next](@next)
